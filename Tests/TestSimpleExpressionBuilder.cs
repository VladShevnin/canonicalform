﻿using CanonicalForm;
using CanonicalForm.Exceptions;
using CanonicalForm.Expression;
using NUnit.Framework;

namespace Tests
{
    public class TestSimpleExpressionBuilder
    {
        [Test]
        public void TestExpression()
        {
            SimpleExpressionBuilder builder = new SimpleExpressionBuilder();
            SumExpression expr1 = builder.CreateExpression("x^2 + 3.5xy + y = y^2 - xy + y").Simplify();
            Assert.AreEqual("x^2-y^2+4.5xy", expr1.ToString());

            SumExpression expr2 = builder.CreateExpression("x^2 = x^2").Simplify();            
            Assert.AreEqual("0", expr2.ToString());

            SumExpression expr3 = builder.CreateExpression("1 = 2").Simplify();
            Assert.AreEqual("-1", expr3.ToString());

            SumExpression expr4 = builder.CreateExpression("xy - 2yx=0").Simplify();
            Assert.AreEqual("-xy", expr4.ToString());

            SumExpression expr5 = builder.CreateExpression("x^-2=-x^-2").Simplify();
            Assert.AreEqual("2x^-2", expr5.ToString());

            SumExpression expr6 = builder.CreateExpression("xy=yx").Simplify();
            Assert.AreEqual("0", expr6.ToString());

            SumExpression expr7 = builder.CreateExpression("x ^ 2yx + 2yx ^ 3 = 0").Simplify();
            Assert.AreEqual("3x^3y", expr7.ToString());
            
        }

        [Test]
        public void TestException()
        {
            SimpleExpressionBuilder builder = new SimpleExpressionBuilder();
            Assert.Catch<ParsingException>(() => { SumExpression sumExpression = builder.CreateExpression("x^2 + 3.5xy + y y^2 - xy + y"); }, "NotHaveException");
            Assert.Catch<ParsingException>(() => { SumExpression sumExpression = builder.CreateExpression("x^2 + 3.5xy = y = y^2 - xy + y"); }, "NotHaveException");
            Assert.Catch<ParsingException>(() => { SumExpression sumExpression = builder.CreateExpression(" = y^2 - xy + y"); }, "NotHaveException");
            Assert.Catch<ParsingException>(() => { SumExpression sumExpression = builder.CreateExpression("y^2 - xy + y ="); }, "NotHaveException");
            Assert.Catch<ParsingException>(() => { SumExpression sumExpression = builder.CreateExpression("x^2 + 3.5.xy + y = y^2 - xy + y"); }, "NotHaveException");
            Assert.Catch<ParsingException>(() => { SumExpression sumExpression = builder.CreateExpression("x^2 + 3.5xy + y = ^2 - xy + y"); }, "NotHaveException");
            Assert.Catch<ParsingException>(() => { SumExpression sumExpression = builder.CreateExpression("x^2 + 3.5xy + y = y^2^2 - xy + y"); }, "NotHaveException");
        }

    }
}
