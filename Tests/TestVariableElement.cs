﻿using CanonicalForm;
using NUnit.Framework;

namespace Tests
{
    public class TestVariableElement
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestBackVariableOrderToString()
        {
            VariableElement x0 = new VariableElement() { Name = 'x', Power = 0 };
            Assert.AreEqual(0, x0.ToString().Length);

            VariableElement x = new VariableElement() { Name = 'x', Power = 1 };
            Assert.AreEqual("x", x.ToString());

            VariableElement x2 = new VariableElement() { Name = 'x', Power = 2 };
            Assert.AreEqual("x^2", x2.ToString());

        }

    }
}
