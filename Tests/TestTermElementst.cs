using CanonicalForm;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    public class TestTermElementst
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestDirectVariableOrderToString()
        {
            TermElement term = new TermElement();
            string x5y5str = "x^5y^5";
            List<VariableElement> x5y5 = new List<VariableElement>() {
                new VariableElement { Name = 'x', Power = 5 },
                new VariableElement { Name = 'y', Power = 5 }
            };
            term.SetValue(1, x5y5);

            Assert.AreEqual(x5y5str, term.ToString());

            string twox5y5str = "2x^5y^5";
            term.SetValue(2, x5y5);

            Assert.AreEqual(twox5y5str, term.ToString());

        }

        [Test]
        public void TestBackVariableOrderToString()
        {
            TermElement term = new TermElement();
            string x5y5str = "x^5y^5";
            List<VariableElement> y5x5 = new List<VariableElement>() {
                new VariableElement { Name = 'y', Power = 5 },
                new VariableElement { Name = 'x', Power = 5 }
            };
            term.SetValue(1, y5x5);

            Assert.AreEqual(x5y5str, term.ToString());

            string twox5y5str = "2x^5y^5";
            term.SetValue(2, y5x5);

            Assert.AreEqual(twox5y5str, term.ToString());

        }

        [Test]
        public void TestDirectPowerOrderToString()
        {
            TermElement term = new TermElement();
            string x5x3xstr = "x^5x^3x";
            List<VariableElement> x5x3x = new List<VariableElement>() {
                new VariableElement { Name = 'x', Power = 5 },
                new VariableElement { Name = 'x', Power = 3 },
                new VariableElement { Name = 'x', Power = 1 }
            };
            term.SetValue(1, x5x3x);

            Assert.AreEqual(x5x3xstr, term.ToString());

            string x5x3x2str = "3.5x^5x^3x^2";
            List<VariableElement> x5x3x2 = new List<VariableElement>() {
                new VariableElement { Name = 'x', Power = 5 },
                new VariableElement { Name = 'x', Power = 3 },
                new VariableElement { Name = 'x', Power = 2 }
            };
            term.SetValue(3.5, x5x3x2);

            Assert.AreEqual(x5x3x2str, term.ToString());
        }

    }
}