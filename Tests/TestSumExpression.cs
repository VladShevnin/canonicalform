﻿using CanonicalForm;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    public class TestSumExpression
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestSumX()
        {
            TermElement t1 = new TermElement();
            List<VariableElement> x2 = new List<VariableElement>() {
                new VariableElement { Name = 'x', Power = 2 }
            };
            t1.SetValue(1, x2);

            TermElement t2 = new TermElement();
            List<VariableElement> y2 = new List<VariableElement>() {
                new VariableElement { Name = 'y', Power = 2 }
            };
            t2.SetValue(1, y2);

            TermElement t3 = new TermElement();
            List<VariableElement> x22 = new List<VariableElement>() {
                new VariableElement { Name = 'x', Power = 2 }
            };
            t3.SetValue(2, x22);

            SumExpression exp = new SumExpression();
            exp.SetValue(new List<TermElement>() { t1, t2, t3 });

            SumExpression sumExpression = exp.Simplify();

            Assert.AreEqual("3x^2+y^2", sumExpression.ToString());
        }

        [Test]
        public void TestSumXY()
        {
            TermElement t1 = new TermElement();
            List<VariableElement> x2 = new List<VariableElement>() {
                new VariableElement { Name = 'x', Power = 2 },
                new VariableElement { Name = 'y', Power = 3 }
            };
            t1.SetValue(1, x2);

            TermElement t2 = new TermElement();
            List<VariableElement> y2 = new List<VariableElement>() {
                new VariableElement { Name = 'y', Power = 2 }
            };
            t2.SetValue(1, y2);

            TermElement t3 = new TermElement();
            List<VariableElement> x22 = new List<VariableElement>() {
                new VariableElement { Name = 'y', Power = 3 },
                new VariableElement { Name = 'x', Power = 2 }
            };
            t3.SetValue(2, x22);

            SumExpression exp = new SumExpression();
            exp.SetValue(new List<TermElement>() { t1, t2, t3 });

            SumExpression sumExpression = exp.Simplify();

            Assert.AreEqual("3x^2y^3+y^2", sumExpression.ToString());
        }

        [Test]
        public void TestSumXYToZero()
        {
            TermElement t1 = new TermElement();
            List<VariableElement> x2 = new List<VariableElement>() {
                new VariableElement { Name = 'x', Power = 2 },
                new VariableElement { Name = 'y', Power = 3 }
            };
            t1.SetValue(1, x2);

            TermElement t2 = new TermElement();
            List<VariableElement> y2 = new List<VariableElement>() {
                new VariableElement { Name = 'y', Power = 2 }
            };
            t2.SetValue(1, y2);

            TermElement t3 = new TermElement();
            List<VariableElement> x22 = new List<VariableElement>() {
                new VariableElement { Name = 'y', Power = 3 },
                new VariableElement { Name = 'x', Power = 2 }
            };
            t3.SetValue(-2.5, x22);

            TermElement t4 = new TermElement();
            List<VariableElement> x23 = new List<VariableElement>() {
                new VariableElement { Name = 'y', Power = 3 },
                new VariableElement { Name = 'x', Power = 2 }
            };
            t4.SetValue(1.5, x23);

            SumExpression exp = new SumExpression();
            exp.SetValue(new List<TermElement>() { t1, t2, t3, t4 });

            SumExpression sumExpression = exp.Simplify();

            Assert.AreEqual("y^2", sumExpression.ToString());


        }


    }
}
