﻿using System;
using System.Collections.Generic;

namespace CanonicalForm
{
    public class VariableElement : IComparer<VariableElement>
    {
        public char Name { get; set; }

        public int Power { get; set; }

        public int Compare(VariableElement x, VariableElement y)
        {
            if (x.Name != y.Name)
                return x.Name.CompareTo(y.Name);

            return y.Power.CompareTo(x.Power);
        }

        public override string ToString()
        {
            if (Power == 0)
                return String.Empty;
            if (Power == 1)
                return $"{Name}";
            return $"{Name}^{Power}";
        }

    }
}
