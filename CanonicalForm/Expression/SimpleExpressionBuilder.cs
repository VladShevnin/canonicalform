﻿using CanonicalForm.Exceptions;
using System;
using System.Globalization;
using System.Text;

namespace CanonicalForm.Expression
{
    public class SimpleExpressionBuilder
    {
        public SumExpression CreateExpression(string s)
        {
            if (!s.Contains('='))
                throw new ParsingException("В исходном выражении нет знака '='");
            string[] v = s.Split("=");
            if (v.Length > 2)
                throw new ParsingException("Знак '=' встречается больше одного раза");
            string leftExpress = v[0].Replace(" ", "");
            string rightExpress = v[1].Replace(" ", "");

            if (leftExpress.Length == 0)
                throw new ParsingException("Не задана левая часть выражение");
            if (rightExpress.Length == 0)
                throw new ParsingException("Не задана правая часть выражение");

            SumExpression leftResult = CreateSumExpression(leftExpress);
            SumExpression rightResult = CreateSumExpression(rightExpress);

            foreach (var item in rightResult.Terms)
            {
                item.Multiplier *= -1;
                leftResult.Terms.Add(item);
            }
            leftResult.Normalize();

            return leftResult;
        }

        private SumExpression CreateSumExpression(string s)
        {
            SumExpression result = new SumExpression();

            bool isNumOrVar = false;
            TermElement newTerm = new TermElement();
            int lastSign = 1;
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsNumber(s[i]))
                {
                    i = ReadNumberToTerm(s, i, newTerm, lastSign) - 1;
                    isNumOrVar = true;
                }
                else
                {
                    switch (s[i])
                    {
                        case '^':
                            i = ReadPowerToTerm(s, i, newTerm) - 1;
                            break;
                        case '-':
                            lastSign = -1;
                            if (isNumOrVar)
                            {
                                result.Terms.Add(newTerm);
                                newTerm = new TermElement();
                                i--;
                                isNumOrVar = false;
                            }
                            break;
                        case '+':
                            lastSign = 1;
                            if (isNumOrVar)
                            {
                                result.Terms.Add(newTerm);
                                newTerm = new TermElement();
                                isNumOrVar = false;
                            }
                            break;
                        default:
                            i = ReadVariableToTerm(s, i, newTerm, lastSign) - 1;
                            isNumOrVar = true;
                            break;
                    }
                }
            }
            if (isNumOrVar)
            {
                result.Terms.Add(newTerm);
            }

            return result;
        }

        private int ReadPowerToTerm(string s, int start, TermElement newTerm)
        {
            StringBuilder numberBuilder = new StringBuilder();            
            for (int i = start + 1; i < s.Length; i++)
            {
                if (Char.IsNumber(s[i]) || ((i == start + 1) && (s[i] == '-')))
                {
                    numberBuilder.Append(s[i]);
                }
                else
                {
                    ParseTermPower(s, start, numberBuilder.ToString(), newTerm);

                    return i;
                }
            }

            string number = numberBuilder.ToString();
            if (number.Length > 0)
            {
                ParseTermPower(s, start, number, newTerm);
            }
            return s.Length;

        }


        private int ReadVariableToTerm(string s, int i, TermElement newTerm, int sign)
        {
            if (newTerm.Multiplier == null)
                newTerm.Multiplier = sign;
            newTerm.Variables.Add(new VariableElement() { Name = s[i], Power = 1 });
            return i + 1;
        }

        private int ReadNumberToTerm(string s, int start, TermElement newTerm, int sign)
        {
            bool isWasPoint = false;
            StringBuilder number = new StringBuilder();
            for (int i = start; i < s.Length; i++)
            {
                if (Char.IsNumber(s[i]) || (i == start && (s[i] == '+' || s[i] == '-')))
                {
                    number.Append(s[i]);
                }
                else if (s[i] == '.')
                {
                    if (isWasPoint)
                        throw new ParsingException($"При парсинге числа {s.Substring(start)} обнаружилась повторная точка");
                    isWasPoint = true;
                    number.Append(s[i]);
                }
                else
                {
                    ParseTermMultiplier(number.ToString(), newTerm, sign);
                    return i;
                }
            }
            ParseTermMultiplier(number.ToString(), newTerm, sign);
            return s.Length;
        }

        private void ParseTermMultiplier(string number, TermElement newTerm, int sign)
        {
            double k = 0;
            if (!double.TryParse(number.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out k))
                throw new ParsingException($"Ошибка при парсинге числа {number.ToString()}");
            newTerm.Multiplier = k * sign;
        }

        private void ParseTermPower(string s, int start, string number, TermElement newTerm)
        {
            if (newTerm.Variables.Count < 1)
                throw new ParsingException($"Чтение показателя степени без переменной {s.Substring(start)}");
            VariableElement lastItem = newTerm.Variables[newTerm.Variables.Count - 1];
            if (lastItem.Power != 1)
                throw new ParsingException($"Повторное чтение показателя степени для переменной {s.Substring(start)}");

            int k = 0;
            if (!int.TryParse(number, out k))
                throw new ParsingException($"Ошибка при парсинге целого числа {number.ToString()}");

            lastItem.Power = k;
        }

    }
}
