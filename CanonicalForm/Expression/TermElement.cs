﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace CanonicalForm
{
    public class TermElement : IComparer<TermElement>
    {
        List<VariableElement> variables = new List<VariableElement>();

        public double? Multiplier { get; set; } = null;

        public List<VariableElement> Variables
        {
            get
            {
                return variables;
            }
        }

        public int Compare(TermElement x, TermElement y)
        {
            int maxPowerX = x.Variables.Count > 0 ? x.Variables.Max(x => x.Power) : 0;
            int maxPowerY = y.Variables.Count > 0 ? y.Variables.Max(x => x.Power) : 0;

            if (maxPowerX != maxPowerY)
                return maxPowerY.CompareTo(maxPowerX);

            int minCount = Math.Min(x.Variables.Count, y.Variables.Count);
            for (int i = 0; i < minCount; i++)
            {
                if (x.Variables[i].Name != y.Variables[i].Name)
                {
                    return x.Variables[i].Name.CompareTo(y.Variables[i].Name);
                }

                if (x.Variables[i].Power != y.Variables[i].Power)
                {
                    return y.Variables[i].Power.CompareTo(x.Variables[i].Power);
                }
            }
            return x.Variables.Count.CompareTo(y.Variables.Count);
        }

        public void Normalize()
        {
            Variables.Sort(new VariableElement());
        }

        public void Simplify()
        {
            List<VariableElement> result = new List<VariableElement>();
            VariableElement lastItem = null;
            foreach (VariableElement curItem in Variables)
            {
                if (lastItem == null)
                {
                    lastItem = curItem;
                }
                else
                {
                    if ((lastItem.Name.Equals(curItem.Name)))
                    {
                        lastItem.Power += curItem.Power;
                    }
                    else
                    {
                        result.Add(lastItem);
                        lastItem = curItem;
                    }
                }
            }
            if (lastItem != null)
            {
                result.Add(lastItem);
            }
            variables = result;
        }

        public void SetValue(double multiplier, List<VariableElement> variables)
        {
            Multiplier = multiplier;
            this.variables = variables;
            this.variables.Sort(new VariableElement());
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            if (Multiplier != null && variables.Count == 0 )
            {
                builder.Append(Multiplier);
            }
            else
            {
                if (Multiplier != null && Multiplier != 1 && Multiplier != -1)
                    builder.Append(Multiplier?.ToString("G", CultureInfo.InvariantCulture));
                if (Multiplier == -1)
                    builder.Append("-");
                foreach (var v in variables)
                {
                    builder.Append(v.ToString());
                }
            }
            return builder.ToString();
        }
    }
}
