﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CanonicalForm
{
    public class SumExpression
    {
        public const double Eps = 0.00001;

        List<TermElement> terms = new List<TermElement>();


        public void SetValue(List<TermElement> terms)
        {
            this.terms = terms;
            Normalize();
        }

        public List<TermElement> Terms
        {
            get
            {
                return terms;
            }
        }

        public void Normalize()
        {
            foreach(var item in this.terms)
            {
                item.Normalize();
                item.Simplify();
            }
            this.terms.Sort(new TermElement());
        }

        public SumExpression Simplify()
        {
            List<TermElement> simExpr = new List<TermElement>();

            TermElement lastTerm = null;
            foreach (TermElement curTerm in terms)
            {
                if (lastTerm == null)
                {
                    lastTerm = curTerm;
                }
                else
                {
                    if ((lastTerm.Compare(lastTerm, curTerm) == 0))
                    {
                        lastTerm.Multiplier += curTerm.Multiplier;
                    }
                    else
                    {
                        if (Math.Abs(lastTerm.Multiplier ?? 0) > Eps)
                        {
                            simExpr.Add(lastTerm);
                        }
                        lastTerm = curTerm;
                    }
                }
            }
            if (lastTerm != null && (Math.Abs(lastTerm.Multiplier ?? 0) > Eps))
            {
                simExpr.Add(lastTerm);
            }

            if(simExpr.Count == 0)
            {
                simExpr.Add(new TermElement() { Multiplier = 0});
            }

            SumExpression result = new SumExpression();
            result.SetValue(simExpr);

            return result;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            bool isFirst = true;
            foreach (TermElement curTerm in terms)
            {
                if (!isFirst && curTerm.Multiplier > 0)
                    builder.Append("+");
                builder.Append(curTerm.ToString());
                isFirst = false;
            }
            return builder.ToString();
        }
    }
}
