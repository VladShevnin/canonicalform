﻿using CanonicalForm.Exceptions;
using CanonicalForm.Expression;
using System;
using System.IO;
using System.Text;

namespace CanonicalForm
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleExpressionBuilder builder = new SimpleExpressionBuilder();
            
            StreamReader sr = null;
            StreamWriter sw = null;
            try
            {
                if (args.Length > 0)
                {
                    sr = new StreamReader(args[0], Encoding.ASCII);
                    sw = new StreamWriter(Path.ChangeExtension(args[0], ".out"), false, Encoding.ASCII);

                    Console.SetOut(sw);
                    Console.SetIn(sr);
                }

                string line;
                while ((line = Console.ReadLine()) != null)
                {
                    try
                    {
                        Console.WriteLine($"{builder.CreateExpression(line).Simplify().ToString()} = 0");
                    }
                    catch (ParsingException ex)
                    {
                        Console.WriteLine($"Ошибка разбора выражения. {ex.Message}");
                    }
                }
            }catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Dispose();
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }
    }
}
