﻿using System;

namespace CanonicalForm.Exceptions
{
    public class ParsingException : Exception
    {
        public ParsingException(string message): base(message)
        {
        }
    }
}
